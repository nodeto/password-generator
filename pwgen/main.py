"""Process html routes."""

import pathlib
from typing import Union

from fastapi import FastAPI, Header, Query
from fastapi.responses import HTMLResponse, JSONResponse, PlainTextResponse

import pwgen.pw

app = FastAPI()


@app.get("/")
async def root() -> HTMLResponse:
    """Return the HTML."""
    return HTMLResponse(
        pathlib.Path(__file__)
        .parent.joinpath("password.html")
        .read_text(encoding="utf-8")
    )


@app.get("/getpw", response_model=None)
async def password(
    hx_request: Union[str, None] = Header(default=None),
    min_length: int = Query(ge=0),  # ge=1 ensures the length can't be less than 1
    max_length: int = Query(le=16),
) -> PlainTextResponse | JSONResponse:
    """Return a generated password."""
    if hx_request:
        return PlainTextResponse(
            pwgen.pw.getpw(min_length=min_length, max_length=max_length)
        )
    return JSONResponse(
        {
            "generatedPassword": pwgen.pw.getpw(
                min_length=min_length, max_length=max_length
            )
        }
    )
