"""Generate a password."""

import itertools
import pathlib
import secrets
import string
from typing import List

import yaml

PHONEMES = yaml.safe_load(
    pathlib.Path(__file__).parent.joinpath("phonemes.yml").read_text(encoding="utf-8")
)
VOWELS = yaml.safe_load(
    pathlib.Path(__file__).parent.joinpath("phonemes.yml").read_text(encoding="utf-8")
)


def get_random_phonemes(number_of_phonemes: int = 4) -> List[str]:
    """Return a list of random phonemes using a CVCV pattern.

    Args:
        number_of_phonemes: The number of phonemes to generate. Defaults to 4.

    Returns:
        A list of strings, where each string is a phoneme.

    Examples:
    >>> import random
    >>> random.seed(0)  # Setting seed to make the output predictable
    >>> secrets.choice = random.choice  # random.choice for predictability
    >>> get_random_phonemes(4)
    ['n', 'ay', 'c', 'u']
    """
    phonemes = []
    phoneme_type_iter = itertools.cycle(["consonants", "vowels"])
    while len(phonemes) < number_of_phonemes:
        phonemes.append(secrets.choice(PHONEMES[next(phoneme_type_iter)]))
    return phonemes


def reduce_consecutive_characters(s: str) -> str:
    """Deduplicate consecutive repeated characters.

    Args:
        s: The input string.

    Returns:
        A string with consecutive repeated characters reduced to a single character.

    Examples:
    >>> reduce_consecutive_characters('aaabbcc')
    'abc'
    >>> reduce_consecutive_characters('hellooo')
    'helo'
    >>> reduce_consecutive_characters('')
    ''
    """
    if not s:
        return s  # Return empty string if input is empty

    # Initialize the reduced string with the first character of the input string
    reduced_string = s[0]

    # Iterate over the string starting from the second character
    for char in s[1:]:
        if char != reduced_string[-1]:
            reduced_string += char

    return reduced_string


def getpw(min_length: int = 11, max_length: int = 16) -> str:
    """Generate a random password based on phonetic patterns.

    The password is constructed by generating two phonetic components, capitalizing the
    first, concatenating them with a hyphen, appending two random digits, and ensuring
    the length falls within the specified range.

    Args:
        min_length: The minimum acceptable length of the generated password.
            Defaults to 11.
        max_length: The maximum acceptable length of the generated password.
            Defaults to 16.

    Returns:
        A string representing the randomly generated password.

    Doctest:
        >>> result = getpw()
        >>> 11 <= len(result) <= 16
        True
        >>> '-' in result
        True
        >>> any(char.isdigit() for char in result)
        True
    """
    password = ""
    while not min_length <= len(password) <= max_length:
        words_for_pw: List[str] = [
            reduce_consecutive_characters("".join(get_random_phonemes()))
            for _ in range(2)
        ]

        digits = "".join(secrets.choice(string.digits) for _ in range(2))

        password = f"{words_for_pw[0].capitalize()}-{words_for_pw[1].lower()}{digits}"
    return password
