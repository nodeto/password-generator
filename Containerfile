FROM docker-proxy.nodeto.site/python:3 AS build-env
RUN groupadd -r pwgen -g 50000 && useradd -r -m -d /app -g pwgen -u 50000 pwgen

WORKDIR /app
COPY pyproject.toml poetry.lock /app/
RUN chown -R pwgen:pwgen /app
USER pwgen
ENV PATH=/app/.local/bin:$PATH

RUN curl -sSL https://install.python-poetry.org | python3 -

RUN poetry export > requirements.txt
RUN pip install --no-cache \
    --user \
    -r requirements.txt \
    --no-deps \
    --index-url https://nexus.nodeto.site/repository/pypi/simple/

FROM docker-proxy.nodeto.site/python:3-slim
COPY --from=build-env /app /app
COPY --from=build-env /etc/passwd /etc/passwd
COPY --from=build-env /etc/group /etc/group

USER pwgen
WORKDIR /app
COPY --chown=pwgen:pwgen . /app
ENV PATH=/app/.local/bin:$PATH
ENV CONTAINERIZED=true

CMD ["uvicorn", "--host=0.0.0.0", "pwgen.main:app"]
