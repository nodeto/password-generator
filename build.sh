#!/bin/bash

set -e
ruff format .
ruff check --fix .
docker build . -t kjoyce77/pwgen:"$(poetry version -s)"
docker tag kjoyce77/pwgen:"$(poetry version -s)" kjoyce77/pwgen
docker push kjoyce77/pwgen:"$(poetry version -s)"
docker push kjoyce77/pwgen
