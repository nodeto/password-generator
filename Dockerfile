FROM python:3.12 as base

ENV PATH=/root/.local/bin:$PATH
RUN pip install --no-cache --user pipx && pipx install poetry==1.7.1

WORKDIR /usr/src/app

COPY . .
RUN poetry build --format=wheel

###
# FINAL
###
FROM python:3.12 as final

COPY --from=base /usr/src/app/dist/. /tmp/
RUN pip install --no-cache /tmp/*

RUN groupadd -g 8961017 pwgen
RUN useradd -u 8961017 -g 8961017 pwgen

USER pwgen

ENTRYPOINT ["uvicorn", "--host", "0.0.0.0", "pwgen.main:app"]
